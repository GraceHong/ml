close all;
clear;
clc;
format compact;

% loading wine dataset, including classnumber = 3, wine is a 178*13 matrix,wine_labes is a 178*1 colum vector
load chapter_WineClass.mat;

% visual image for dataset
figure;
boxplot(wine,'orientation','horizontal','labels',categories);
title('the box image of wine','FontSize',12);
xlabel('attributes','FontSize',12);
grid on;

figure
subplot(3,5,1);
hold on
for run = 1:178
    plot(run,wine_labels(run),'*');
end
xlabel('sample','FontSize',10);
ylabel('class label','FontSize',10);
title('class','FontSize',10);
for run = 2:14
    subplot(3,5,run);
    hold on;
    str = ['attrib ',num2str(run-1)];
    for i = 1:178
        plot(i,wine(i,run-1),'*');
    end
    xlabel('sample','FontSize',10);x
    ylabel('attribute value','FontSize',10);
    title(str,'FontSize',10);
end

% select training and testing dataset

% select 1-30 from class1,select 60-95 from class2, select 131-153 from class3 as training dataset
train_wine = [wine(1:30,:);wine(60:95,:);wine(131:153,:)];
% gather the label from training dataset
train_wine_labels = [wine_labels(1:30);wine_labels(60:95);wine_labels(131:153)];
% select 31-59 from class1, select 96-130 from class2, select 154-178 from class3 as testing dataset
test_wine = [wine(31:59,:);wine(96:130,:);wine(154:178,:)];
% gather the label from testing dataset
test_wine_labels = [wine_labels(31:59);wine_labels(96:130);wine_labels(154:178)];

% preprocessing data, normalize training dataset and testing dataset to [0,1]

[mtrain,ntrain] = size(train_wine);
[mtest,ntest] = size(test_wine);

dataset = [train_wine;test_wine];
% call mapminmax from MATLAB library
[dataset_scale,ps] = mapminmax(dataset',0,1);
dataset_scale = dataset_scale';

train_wine = dataset_scale(1:mtrain,:);
test_wine = dataset_scale( (mtrain+1):(mtrain+mtest),: );
%% SVM training
tic;
model = svmtrain(train_wine_labels, train_wine, '-c 2 -g 1');
toc;
%% SVM testing
tic;
[predict_label, accuracy,dec_value1] = svmpredict(test_wine_labels, test_wine, model);
toc;

%% analyse the result

% the reslut of Classify and real Class(testing dataset)
% only one sample is misclassificated
figure;
hold on;
plot(test_wine_labels,'o');
plot(predict_label,'r*');
xlabel('sample','FontSize',12);
ylabel('True label','FontSize',12);
legend('True lable','Predicted label');
title('Reslut of Classify','FontSize',12);
grid on;
